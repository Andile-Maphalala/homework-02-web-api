USE [master]
GO
/****** Object:  Database [Homework 01-02]    Script Date: 2020/03/15 23:07:27 ******/
CREATE DATABASE [Homework 01-02]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Homework 01', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\Homework 01.mdf' , SIZE = 4288KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Homework 01_log', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\Homework 01_log.ldf' , SIZE = 1072KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Homework 01-02] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Homework 01-02].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Homework 01-02] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Homework 01-02] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Homework 01-02] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Homework 01-02] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Homework 01-02] SET ARITHABORT OFF 
GO
ALTER DATABASE [Homework 01-02] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Homework 01-02] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Homework 01-02] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Homework 01-02] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Homework 01-02] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Homework 01-02] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Homework 01-02] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Homework 01-02] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Homework 01-02] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Homework 01-02] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Homework 01-02] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Homework 01-02] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Homework 01-02] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Homework 01-02] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Homework 01-02] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Homework 01-02] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Homework 01-02] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Homework 01-02] SET RECOVERY FULL 
GO
ALTER DATABASE [Homework 01-02] SET  MULTI_USER 
GO
ALTER DATABASE [Homework 01-02] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Homework 01-02] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Homework 01-02] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Homework 01-02] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Homework 01-02] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Homework 01-02', N'ON'
GO
USE [Homework 01-02]
GO
/****** Object:  Table [dbo].[Album]    Script Date: 2020/03/15 23:07:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Album](
	[AlbumID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](50) NOT NULL,
	[Rating] [int] NULL,
	[ArtistID] [int] NULL,
 CONSTRAINT [PK_Album] PRIMARY KEY CLUSTERED 
(
	[AlbumID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Artist]    Script Date: 2020/03/15 23:07:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Artist](
	[ArtistID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Surname] [varchar](50) NULL,
	[UserID] [int] NULL,
	[Email] [varchar](50) NULL,
	[CellphoneNo] [varchar](10) NULL,
 CONSTRAINT [PK_Artists] PRIMARY KEY CLUSTERED 
(
	[ArtistID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tracks]    Script Date: 2020/03/15 23:07:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tracks](
	[TrackID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](50) NOT NULL,
	[Genre] [varchar](50) NOT NULL,
	[Price] [float] NOT NULL,
	[AlbumID] [int] NULL,
 CONSTRAINT [PK_Tracks] PRIMARY KEY CLUSTERED 
(
	[TrackID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 2020/03/15 23:07:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[Username] [varchar](50) NULL,
	[Password] [varchar](max) NULL,
	[UserTypeID] [int] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserType]    Script Date: 2020/03/15 23:07:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserType](
	[UserTypeID] [int] IDENTITY(1,1) NOT NULL,
	[UserTypeDescription] [char](10) NOT NULL,
 CONSTRAINT [PK_UserType] PRIMARY KEY CLUSTERED 
(
	[UserTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Album] ON 

INSERT [dbo].[Album] ([AlbumID], [Title], [Rating], [ArtistID]) VALUES (2, N'NOIR', 8, 3)
INSERT [dbo].[Album] ([AlbumID], [Title], [Rating], [ArtistID]) VALUES (3, N'The Color Of You', 8, 5)
INSERT [dbo].[Album] ([AlbumID], [Title], [Rating], [ArtistID]) VALUES (4, N'Sensitive Soul', 10, 2)
INSERT [dbo].[Album] ([AlbumID], [Title], [Rating], [ArtistID]) VALUES (5, N'Starry Ache', 8, 2)
INSERT [dbo].[Album] ([AlbumID], [Title], [Rating], [ArtistID]) VALUES (6, N'Tetsuo and Youth', 10, 4)
INSERT [dbo].[Album] ([AlbumID], [Title], [Rating], [ArtistID]) VALUES (7, N'Midnight Moonlight', 10, 1)
SET IDENTITY_INSERT [dbo].[Album] OFF
SET IDENTITY_INSERT [dbo].[Artist] ON 

INSERT [dbo].[Artist] ([ArtistID], [Name], [Surname], [UserID], [Email], [CellphoneNo]) VALUES (1, N'Ravyn Lenae', N'gdfgd', 3, N'cdfgdgd', N'dgdgdgdg')
INSERT [dbo].[Artist] ([ArtistID], [Name], [Surname], [UserID], [Email], [CellphoneNo]) VALUES (2, N'Hope Tala', N'fddgfdg', 9, N'gdfgd', N'dfgdgdg')
INSERT [dbo].[Artist] ([ArtistID], [Name], [Surname], [UserID], [Email], [CellphoneNo]) VALUES (3, N'Smino', N'dgsgdfg', 10, N'hjg', N'ghjgj')
INSERT [dbo].[Artist] ([ArtistID], [Name], [Surname], [UserID], [Email], [CellphoneNo]) VALUES (4, N'Lupe Fiasco', N'cbbfh', 11, N'gjgj', N'gjgj')
INSERT [dbo].[Artist] ([ArtistID], [Name], [Surname], [UserID], [Email], [CellphoneNo]) VALUES (5, N'Alina Baraz', N'Pleasse', 9, N'gjjfgj', N'vbnn')
SET IDENTITY_INSERT [dbo].[Artist] OFF
SET IDENTITY_INSERT [dbo].[Tracks] ON 

INSERT [dbo].[Tracks] ([TrackID], [Title], [Genre], [Price], [AlbumID]) VALUES (2, N'Blue', N'R&B/Soul', 100, 5)
INSERT [dbo].[Tracks] ([TrackID], [Title], [Genre], [Price], [AlbumID]) VALUES (3, N'Valenttine', N'R&B/Soul', 100, 5)
INSERT [dbo].[Tracks] ([TrackID], [Title], [Genre], [Price], [AlbumID]) VALUES (4, N'Moontime', N'R&B/Soul', 100, 5)
INSERT [dbo].[Tracks] ([TrackID], [Title], [Genre], [Price], [AlbumID]) VALUES (5, N'Jealous', N'Alternative', 98, 4)
INSERT [dbo].[Tracks] ([TrackID], [Title], [Genre], [Price], [AlbumID]) VALUES (6, N'Lovestained', N'Alternative', 95, 4)
INSERT [dbo].[Tracks] ([TrackID], [Title], [Genre], [Price], [AlbumID]) VALUES (7, N'D.T.M', N'Alternative', 100, 4)
INSERT [dbo].[Tracks] ([TrackID], [Title], [Genre], [Price], [AlbumID]) VALUES (8, N'Anywhere', N'Alternative', 96, 4)
INSERT [dbo].[Tracks] ([TrackID], [Title], [Genre], [Price], [AlbumID]) VALUES (12, N'KRUSH', N'Hip-Hop/Rap', 91, 2)
INSERT [dbo].[Tracks] ([TrackID], [Title], [Genre], [Price], [AlbumID]) VALUES (13, N'MERLOT', N'Hip-Hop/Rap', 98, 2)
INSERT [dbo].[Tracks] ([TrackID], [Title], [Genre], [Price], [AlbumID]) VALUES (14, N'VERIZON', N'Hip-Hop/Rap', 93, 2)
INSERT [dbo].[Tracks] ([TrackID], [Title], [Genre], [Price], [AlbumID]) VALUES (20, N'Floating', N'R&B/Soul', 99, 3)
INSERT [dbo].[Tracks] ([TrackID], [Title], [Genre], [Price], [AlbumID]) VALUES (21, N'Electric', N'R&B/Soul', 91, 3)
INSERT [dbo].[Tracks] ([TrackID], [Title], [Genre], [Price], [AlbumID]) VALUES (22, N'Yours', N'R&B/Soul', 100, 3)
INSERT [dbo].[Tracks] ([TrackID], [Title], [Genre], [Price], [AlbumID]) VALUES (23, N'Mural', N'Hip-Hop/Rap', 100, 6)
INSERT [dbo].[Tracks] ([TrackID], [Title], [Genre], [Price], [AlbumID]) VALUES (24, N'Dots and Lines', N'Hip-Hop/Rap', 100, 6)
INSERT [dbo].[Tracks] ([TrackID], [Title], [Genre], [Price], [AlbumID]) VALUES (25, N'Deliver', N'Hip-Hop/Rap', 100, 6)
INSERT [dbo].[Tracks] ([TrackID], [Title], [Genre], [Price], [AlbumID]) VALUES (26, N'Prisoner 1 and 2', N'Hip-Hop/Rap', 100, 6)
INSERT [dbo].[Tracks] ([TrackID], [Title], [Genre], [Price], [AlbumID]) VALUES (27, N'Little Death', N'Hip-Hop/Rap', 100, 6)
INSERT [dbo].[Tracks] ([TrackID], [Title], [Genre], [Price], [AlbumID]) VALUES (28, N'Adoration of the Magi', N'Hip-Hop/Rap', 98, 6)
INSERT [dbo].[Tracks] ([TrackID], [Title], [Genre], [Price], [AlbumID]) VALUES (1041, N'Work', N'R&B/Soul', 500, 5)
SET IDENTITY_INSERT [dbo].[Tracks] OFF
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([UserID], [Username], [Password], [UserTypeID]) VALUES (3, N'Reason', N'12345', 1)
INSERT [dbo].[User] ([UserID], [Username], [Password], [UserTypeID]) VALUES (9, N'Hota', N'NeverHappy', NULL)
INSERT [dbo].[User] ([UserID], [Username], [Password], [UserTypeID]) VALUES (10, N'Smi', N'Anita', 2)
INSERT [dbo].[User] ([UserID], [Username], [Password], [UserTypeID]) VALUES (11, N'LuFiasco', N'Bestrapper', 2)
INSERT [dbo].[User] ([UserID], [Username], [Password], [UserTypeID]) VALUES (12, N'AlBara', N'IamChill', 2)
INSERT [dbo].[User] ([UserID], [Username], [Password], [UserTypeID]) VALUES (2018, N'itworks', N'yep it does', 2)
INSERT [dbo].[User] ([UserID], [Username], [Password], [UserTypeID]) VALUES (3014, N'hmmmmm', N'741', 1)
INSERT [dbo].[User] ([UserID], [Username], [Password], [UserTypeID]) VALUES (3017, N'yay', N'323123', 1)
SET IDENTITY_INSERT [dbo].[User] OFF
SET IDENTITY_INSERT [dbo].[UserType] ON 

INSERT [dbo].[UserType] ([UserTypeID], [UserTypeDescription]) VALUES (1, N'Admins    ')
INSERT [dbo].[UserType] ([UserTypeID], [UserTypeDescription]) VALUES (2, N'Artist    ')
SET IDENTITY_INSERT [dbo].[UserType] OFF
/****** Object:  Index [fkIdx_25]    Script Date: 2020/03/15 23:07:27 ******/
CREATE NONCLUSTERED INDEX [fkIdx_25] ON [dbo].[Album]
(
	[ArtistID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [fkIdx_28]    Script Date: 2020/03/15 23:07:27 ******/
CREATE NONCLUSTERED INDEX [fkIdx_28] ON [dbo].[Tracks]
(
	[AlbumID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Album]  WITH CHECK ADD  CONSTRAINT [FK_25] FOREIGN KEY([ArtistID])
REFERENCES [dbo].[Artist] ([ArtistID])
GO
ALTER TABLE [dbo].[Album] CHECK CONSTRAINT [FK_25]
GO
ALTER TABLE [dbo].[Artist]  WITH CHECK ADD  CONSTRAINT [FK_Artist_User] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[Artist] CHECK CONSTRAINT [FK_Artist_User]
GO
ALTER TABLE [dbo].[Tracks]  WITH CHECK ADD  CONSTRAINT [FK_28] FOREIGN KEY([AlbumID])
REFERENCES [dbo].[Album] ([AlbumID])
GO
ALTER TABLE [dbo].[Tracks] CHECK CONSTRAINT [FK_28]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_UserType] FOREIGN KEY([UserTypeID])
REFERENCES [dbo].[UserType] ([UserTypeID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_UserType]
GO
USE [master]
GO
ALTER DATABASE [Homework 01-02] SET  READ_WRITE 
GO
