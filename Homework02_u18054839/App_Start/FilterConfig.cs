﻿using System.Web;
using System.Web.Mvc;

namespace Homework02_u18054839
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
