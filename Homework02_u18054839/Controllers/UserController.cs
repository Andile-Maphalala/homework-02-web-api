﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Homework02_u18054839.Models;
using System.Web.Mvc;
using System.Dynamic;


namespace Homework01_18054839.Controllers
{
    public class UserController : ApiController
    {

        [System.Web.Http.Route("api/User/GetAllUsers")]
        [System.Web.Mvc.HttpPost]
        public List<User> GetAllUsers()
        {
            Entities db = new Entities();
            db.Configuration.ProxyCreationEnabled = false;
            return db.Users.ToList();
        }

       


        /// ///////////////////////////////////////////////////////////////////////////////////////////

        [System.Web.Http.Route("api/User/AddUser")]
        [System.Web.Mvc.HttpPost]
        public User AddUser(User user)
        {
            
                Entities db = new Entities();
                db.Configuration.ProxyCreationEnabled = false;
                db.Users.Add(user);
                db.SaveChanges();
                return user;
        }






        //////////////////////////////////////////////////////////////////////////////////////////////////

        [System.Web.Http.Route("api/User/UpdateUser")]
        [System.Web.Mvc.HttpPost]
        public User UpdateUser(User user)
        {
            if ( user.UserID == 0)
            {
                return user;
            }
            else {
                int i = 0;
                Entities db = new Entities();
                db.Configuration.ProxyCreationEnabled = false;
                User Update = new User();
                Update = db.Users.Where(zz => zz.UserID == user.UserID).FirstOrDefault();
                Update.UserID = user.UserID;
                Update.Username = user.Username;
                Update.Password = user.Password;
                Update.UserTypeID = user.UserTypeID;

                db.SaveChanges();

                i++;
            }
            return user;
        }


        /// //////////////////////////////////////////////////////////

        [System.Web.Http.Route("api/User/GetUserById/{UserID}")]
        [System.Web.Mvc.HttpPost]
        public User GetUserById(int UserID)
        {
            Entities db = new Entities();
            db.Configuration.ProxyCreationEnabled = false;
           User UserObj = new User();
            int ID = Convert.ToInt32(UserID);
            try
            {
                UserObj = db.Users.Find(ID);

            }
            catch (Exception)
            {
                throw;
            }

            return UserObj;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////

        [System.Web.Http.Route("api/User/DeleteUser/{UserID}")]
        [System.Web.Mvc.HttpPost]
        public User DeleteUser(int  UserID)
        {
            Entities db = new Entities();
            db.Configuration.ProxyCreationEnabled = false;
            User Delete = new User();
            Delete = db.Users.Where(zz => zz.UserID == UserID).FirstOrDefault();

           
            db.Users.Remove(Delete);
            db.SaveChanges();

            return Delete;
        }


    }


}
