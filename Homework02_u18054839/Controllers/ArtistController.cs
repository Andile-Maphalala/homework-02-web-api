﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Homework02_u18054839.Models;

namespace Homework02_u18054839.Controllers
{
    public class ArtistController : ApiController
    {
        [System.Web.Http.Route("api/Artist/GetAllArtists")]
        [System.Web.Mvc.HttpPost]
        public List<Artist> GetAllArtists()
        {
           Entities  db = new Entities();
            db.Configuration.ProxyCreationEnabled = false;

            //List<Artist> List = db.Artists.ToList();
            //return List;
            try
            {
                return db.Artists.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }




        //    ////////////////////////////////////////////////////

        [System.Web.Http.Route("api/Artist/AddArtist")]
        [System.Web.Mvc.HttpPost]
        public Artist AddArtist(Artist artist)
        {
            Entities db = new Entities();
            db.Configuration.ProxyCreationEnabled = false;
            db.Artists.Add(artist);
            db.SaveChanges();
            return artist;
        }

        ///// //////////////////////////////////////////////////////////////////////////

        [System.Web.Http.Route("api/Artist/UpdateArtist")]
        [System.Web.Mvc.HttpPost]
        public Artist UpdateArtist(Artist artist)
        {

            Entities db = new Entities();
            db.Configuration.ProxyCreationEnabled = false;
            Artist Update = new Artist();
            Update = db.Artists.Where(zz => zz.ArtistID == artist.ArtistID).FirstOrDefault();
            Update.ArtistID = artist.ArtistID;
            Update.Name = artist.Name;
            Update.Surname = artist.Surname;
            Update.CellphoneNo = artist.CellphoneNo;
            Update.Email = artist.Email;

            db.SaveChanges();


            return artist;
        }


        /// ///////////////////////////////////////////////////////////////////////////

        [System.Web.Http.Route("api/Artist/GetArtistById/{ArtistID}")]
        [System.Web.Mvc.HttpPost]
        public Artist GetArtistById(int ArtistID)
        {
            Entities db = new Entities();
            db.Configuration.ProxyCreationEnabled = false;
            Artist ArtistObj = new Artist();
            int ID = Convert.ToInt32(ArtistID);
            try
            {
                ArtistObj = db.Artists.Find(ID);

            }
            catch (Exception)
            {
                throw;
            }

            return ArtistObj;
        }


        //    /////////////////////////////////////////////////////////////////////

        [System.Web.Http.Route("api/Artist/DeleteArtist/{artistID}")]
        [System.Web.Mvc.HttpPost]
        public Artist DeleteArtist(int artistID)
        {
            Entities db = new Entities();
            db.Configuration.ProxyCreationEnabled = false;
            Artist Delete = new Artist();
            Delete = db.Artists.Where(zz => zz.ArtistID == artistID).FirstOrDefault();

            db.Artists.Remove(Delete);
            db.SaveChanges();
            return Delete; ;
        }
    }
}

