﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Homework02_u18054839.Models;
using System.Dynamic;

namespace Homework01_18054839.Controllers
{
    public class TrackController : ApiController
    {
        [System.Web.Http.Route("api/Track/GetAllTracks")]
        [System.Web.Mvc.HttpPost]
        public List<Track> GetAllTracks()
        {
            Entities db = new Entities();
            db.Configuration.ProxyCreationEnabled = false;

            try
            {
                return db.Tracks.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }






        //    ////////////////////////////////////////////////////

        [System.Web.Http.Route("api/Track/AddTrack")]
        [System.Web.Mvc.HttpPost]
        public dynamic AddTrack(Track track)
        {
            Entities db = new Entities();
            db.Configuration.ProxyCreationEnabled = false;
            db.Tracks.Add(track);
            db.SaveChanges();
            return track;
        }
        /////////////////////////////////////////////////////////

        [System.Web.Http.Route("api/Track/UpdateTrack")]
        [System.Web.Mvc.HttpPost]
        public Track UpdateTrack(Track track)
        {

            Entities db = new Entities();
            db.Configuration.ProxyCreationEnabled = false;
            Track Update = new Track();
            Update = db.Tracks.Where(zz => zz.TrackID == track.TrackID).FirstOrDefault();
            Update.TrackID = track.TrackID;
            Update.Title = track.Title;
            Update.Genre = track.Genre;
            Update.Price = track.Price;
            Update.AlbumID = track.AlbumID;

            db.SaveChanges();


            return track;
        }


        /// ///////////////////////////////////////////////////////////////////////////

        [System.Web.Http.Route("api/Track/GetTrackById/{TrackID}")]
        [System.Web.Mvc.HttpPost]
        public Track GettrackById(int TrackID)
        {
            Entities db = new Entities();
            db.Configuration.ProxyCreationEnabled = false;
            Track trackObj = new Track();
            int ID = Convert.ToInt32(TrackID);
            try
            {
                trackObj = db.Tracks.Find(ID);

            }
            catch (Exception)
            {
                throw;
            }

            return trackObj;
        }

        /// <summary>
        /// ///////////////////////////////////////


        [System.Web.Http.Route("api/Track/DeleteTrack/{TrackID}")]
        [System.Web.Mvc.HttpPost]
        public Track DeleteTrack(int TrackID)
        {
            Entities db = new Entities();
            db.Configuration.ProxyCreationEnabled = false;
            Track Delete = new Track();
            Delete = db.Tracks.Where(zz => zz.TrackID == TrackID).FirstOrDefault();


            db.Tracks.Remove(Delete);
            db.SaveChanges();
            return Delete; 
        }
    }
}
