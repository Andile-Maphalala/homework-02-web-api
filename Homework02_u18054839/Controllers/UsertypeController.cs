﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Homework02_u18054839.Models;

namespace Homework01_18054839.Controllers
{
    public class UsertypeController : ApiController
    {
        //[System.Web.Http.Route("api/Usertype/DeleteUsertype")]
        //[System.Web.Mvc.HttpPost]
        //public IHttpActionResult DeleteUsertype(int ID)
        //{
        //    Entities db = new Entities();
        //    db.Configuration.ProxyCreationEnabled = false;
        //    UserType Delete = new UserType();
        //    Delete = db.UserTypes.Where(zz => zz.UserTypeID == ID).FirstOrDefault();



        //    db.UserTypes.Remove(Delete);
        //    db.SaveChanges();
        //    return Ok(Delete); ;
        //}
        /// <summary>
        /// //////////////////////////////////
        /// </summary>
        /// <returns></returns>
        [System.Web.Http.Route("api/Usertype/GetAllUsertypes")]
        [System.Web.Mvc.HttpPost]
        public List<UserType> GetAllUsertypes()
        {
            Entities db = new Entities();
            db.Configuration.ProxyCreationEnabled = false;

            //List<usertype> List = db.usertypes.ToList();
            //return List;
            try
            {
                return db.UserTypes.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

      


        //    ////////////////////////////////////////////////////

        [System.Web.Http.Route("api/Usertype/AddUsertype")]
        [System.Web.Mvc.HttpPost]
        public dynamic AddUsertype(UserType usertype)
        {
            Entities db = new Entities();
            db.Configuration.ProxyCreationEnabled = false;
            db.UserTypes.Add(usertype);
            db.SaveChanges();
            return usertype;
        }

        ///// //////////////////////////////////////////////////////////////////////////

        [System.Web.Http.Route("api/Usertype/UpdateUsertype")]
        [System.Web.Mvc.HttpPost]
        public UserType UpdateUsertype(UserType usertype)
        {

            Entities db = new Entities();
            db.Configuration.ProxyCreationEnabled = false;
            UserType Update = new UserType();
            Update = db.UserTypes.Where(zz => zz.UserTypeID == usertype.UserTypeID).FirstOrDefault();
            Update.UserTypeID = usertype.UserTypeID;
            Update.UserTypeDescription = usertype.UserTypeDescription;

            db.SaveChanges();


            return usertype;
        }


        /// ///////////////////////////////////////////////////////////////////////////

        [System.Web.Http.Route("api/Usertype/GetUsertypeById/{usertypeID}")]
        [System.Web.Mvc.HttpPost]
        public UserType GetUsertypeById(int usertypeID)
        {
            Entities db = new Entities();
            db.Configuration.ProxyCreationEnabled = false;
            UserType usertypeObj = new UserType();
            int ID = Convert.ToInt32(usertypeID);
            try
            {
                usertypeObj = db.UserTypes.Find(ID);

            }
            catch (Exception)
            {
                throw;
            }

            return usertypeObj;
        }


        //    /////////////////////////////////////////////////////////////////////


        [System.Web.Http.Route("api/Usertype/DeleteUsertype/{usertypeID}")]
        [System.Web.Mvc.HttpPost]
        public UserType DeleteUsertype(int usertypeID)
        {
            Entities db = new Entities();
            db.Configuration.ProxyCreationEnabled = false;
            UserType Delete = new UserType();
            Delete = db.UserTypes.Where(zz => zz.UserTypeID == usertypeID).FirstOrDefault();


            db.UserTypes.Remove(Delete);
            db.SaveChanges();

            return Delete;
        }


    }
}

