﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Homework02_u18054839.Models;
using System.Dynamic;
using System.Web.Cors;


namespace Homework02_u18054839.Controllers
{
    public class AlbumController : ApiController
    {
        [System.Web.Http.Route("api/Album/GetAllAlbums")]
        [System.Web.Http.HttpGet]
        public List<Album> GetAllAlbums()
        {
            Entities db = new Entities();
            db.Configuration.ProxyCreationEnabled = false;
            

            //List<Album> List = db.Albums.ToList();
            //return List;
            try
            {
                return db.Albums.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        //  return GetAlbumList(db.Albums.ToList());


        //    private List<dynamic> GetAlbumList(List<Album> ForClient)
        //    {
        //        List<dynamic> DynamicAlbumlists = new List<dynamic>();
        //        foreach (Album album in ForClient)
        //        {
        //            dynamic DynamicAlbum = new ExpandoObject();
        //            DynamicAlbum.AlbumID =album.AlbumID;
        //            DynamicAlbum.Title = album.Title;
        //            DynamicAlbum.Rating = album.Rating;
        //            DynamicAlbum.ArtistID = album.ArtistID;


        //            DynamicAlbumlists.Add(DynamicAlbum);
        //        }

        //        return DynamicAlbumlists;
        //    }


        //    ////////////////////////////////////////////////////

        [Route("api/Album/AddAlbum")]
        [HttpPost]
        public dynamic AddAlbum(Album album)
        {
            Entities db = new Entities();
            db.Configuration.ProxyCreationEnabled = false;
            db.Albums.Add(album);
            db.SaveChanges();
            return album;
        }

        ///// //////////////////////////////////////////////////////////////////////////

        [Route("api/Album/UpdateAlbum")]
        [HttpPost]
        public Album UpdateAlbum(Album album)
        {

            Entities db = new Entities();
            db.Configuration.ProxyCreationEnabled = false;
            Album Update = new Album();
            Update = db.Albums.Where(zz => zz.AlbumID == album.AlbumID).FirstOrDefault();
            Update.AlbumID = album.AlbumID;
            Update.Title = album.Title;
            Update.Rating = album.Rating;
            Update.ArtistID = album.ArtistID;

            db.SaveChanges();

            return album;
            
        }


        /// ///////////////////////////////////////////////////////////////////////////

        [Route("api/Album/GetAlbumById/{AlbumID}")]
        [HttpPost]
        public Album GetAlbumById(int AlbumID)
        {
            Entities db = new Entities();
            db.Configuration.ProxyCreationEnabled = false;
            Album AlbumObj = new Album();
            int ID = Convert.ToInt32(AlbumID);
            try
            {
               AlbumObj = db.Albums.Find(ID);
                
            }
            catch (Exception)
            {
                throw;
            }

            return AlbumObj;
        }


        //    /////////////////////////////////////////////////////////////////////

        [Route("api/Album/DeleteAlbum/{AlbumID}")]
        [HttpPost]
        public Album DeleteAlbum(int AlbumID)
        {
            Entities db = new Entities();
            db.Configuration.ProxyCreationEnabled = false;
            Album Delete = new Album();
            Delete = db.Albums.Where(zz => zz.AlbumID == AlbumID).FirstOrDefault();

            List<Track> Current = new List<Track>();
            Current = db.Tracks.Where(zz => zz.AlbumID == AlbumID).ToList();
            foreach (Track Item in Current)
            {
                db.Tracks.Remove(Item);
            }

            db.Albums.Remove(Delete);
            db.SaveChanges();
           return Delete; 
        }
    }
}




        //}

    

